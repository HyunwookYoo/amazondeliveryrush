using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warehouse : MonoBehaviour
{
    [SerializeField] GameObject[] packages;

    [SerializeField] int maxPackageInLevel = 5;

    [SerializeField] float minSpawnRangeX;

    [SerializeField] float maxSpawnRangeX;

    [SerializeField] float minSpawnRangeY;

    [SerializeField] float maxSpawnRangeY;

    [SerializeField] float spawnTime = 1f;

    public int currentPackageInLevel = 0;

    [SerializeField] Collider2D[] colliders;

    [SerializeField] Vector2 boxSize;

    void Start() 
    {
        StartCoroutine(SpawnPackage());        
    }

    IEnumerator SpawnPackage()
    {
        while (currentPackageInLevel < maxPackageInLevel)
        {
            yield return new WaitForSeconds(spawnTime);
            bool canSpawnHere = false;
            Vector3 position = new Vector3(0f, 0f, 0f);
            int safetyNet = 0;

            while (!canSpawnHere)
            {
                float XCoordinate = Random.Range(minSpawnRangeX, maxSpawnRangeX);
                float YCoordinate = Random.Range(minSpawnRangeY, maxSpawnRangeY);

                position = new Vector3(XCoordinate, YCoordinate);
                canSpawnHere = PreventSpawnOverlap(position);
                safetyNet++;

                if (safetyNet > 50)
                {
                    safetyNet = 0;
                    Debug.Log("Too many attempt!!");
                    break;
                }
            }
            
        
            GameObject package = Instantiate(packages[Random.Range(0, packages.Length)], position, Quaternion.identity);
            currentPackageInLevel++;
        }
    }

    bool PreventSpawnOverlap(Vector3 spawnPos)
    {
        colliders = Physics2D.OverlapBoxAll(transform.position, boxSize, 0f);

        for (int i = 0; i < colliders.Length; i++)
        {
            Vector3 centerPoint = colliders[i].bounds.center;
            float width = colliders[i].bounds.extents.x + 0.4f;
            float height = colliders[i].bounds.extents.y + 0.4f;

            float leftExtent = centerPoint.x - width;
            float rightExtent = centerPoint.x + width;
            float lowerExtent = centerPoint.y - height;
            float upperExtent = centerPoint.y + height;

            if (spawnPos.x <= rightExtent && spawnPos.x >= leftExtent)
            {
                if (spawnPos.y <= upperExtent && spawnPos.y >= lowerExtent)
                {
                    return false;
                }
            }
        }

        return true;
    }
}
