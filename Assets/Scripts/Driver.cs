using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Driver : MonoBehaviour
{
    [SerializeField] float steerSpeed = 250f;
    [SerializeField] float moveSpeed = 20f;
    [SerializeField] float originalMoveSpeed = 20f;
    [SerializeField] float boostMoveSpeed = 30f;
    [SerializeField] float slowedMoveSpeed = 10f;
    [SerializeField] GameObject hitEffect;
    public bool isBoosted = false;
    float duration;
    float timeElapsed;


    void Start()
    {
        
    }

    void Update()
    {
        if (timeElapsed < duration)
        {
            timeElapsed += 1f * Time.deltaTime;
        }
        else 
        {
            moveSpeed = originalMoveSpeed;
            isBoosted = false;
        }

        float steerAmount = Input.GetAxis("Horizontal") * steerSpeed * Time.deltaTime;
        float moveAmount = Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime;
        transform.Rotate(0, 0, -steerAmount);
        transform.Translate(0, moveAmount, 0);
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        if (isBoosted) 
        {
            isBoosted = false;
        }
        timeElapsed = 0f;
        duration = 2f;
        moveSpeed = slowedMoveSpeed;

        foreach (ContactPoint2D contact in other.contacts)
        {
            Instantiate(hitEffect, contact.point, Quaternion.identity);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.tag == "Boost")
        {
            // Debug.Log("Boost Triggered");
            moveSpeed = boostMoveSpeed;
            timeElapsed = 0f;
            duration = 5f;
            isBoosted = true;
            Destroy(other.gameObject, 0f);
        }    
    }

    public float GetBoostPercent()
    {
        return 1f - timeElapsed / duration;
    }
}
