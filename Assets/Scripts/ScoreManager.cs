using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] TMP_Text score;
    [SerializeField] Delivery PlayerDelivery;

    void Start()
    {
        score.SetText("Score: " + PlayerDelivery.currentScore);
    }

    public void UpdateText(int updatedScore)
    {
        score.SetText("Score: " + updatedScore);
    }
}
