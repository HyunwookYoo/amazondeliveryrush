using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoostTimer : MonoBehaviour
{
    [SerializeField] Driver playerDriver;

    Slider boostSlider;

    void Start()
    {
        boostSlider = GetComponent<Slider>();
        boostSlider.value = 0f;
    }

    void Update()
    {
        if (playerDriver.isBoosted)
        {
            boostSlider.value = playerDriver.GetBoostPercent();
        }
        else
        {
            boostSlider.value = 0f;
        }
    }
}
