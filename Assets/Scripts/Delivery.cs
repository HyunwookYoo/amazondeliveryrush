using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Delivery : MonoBehaviour
{
    [SerializeField] Color32 hasPackageColor = new Color32(1, 1, 1, 1);
    [SerializeField] Color32 noPackageColor = new Color32(200, 0, 0, 1);
    [SerializeField] float destroyDelay = 0.5f;
    [SerializeField] ScoreManager ScoreUI;
    bool hasPackage = false;
    SpriteRenderer spriteRenderer;
    string currentPackageTag;

    public int currentScore = 0;

    private void Start() 
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.color = noPackageColor;
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        Debug.Log("Ouch!!!");
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (!other.GetComponent<Customer>() && other.tag != "Boost" && !hasPackage)
        {
            Debug.Log("Package picked up!!");
            hasPackage = true;
            spriteRenderer.color = hasPackageColor;
            currentPackageTag = other.tag;
            Destroy(other.gameObject, destroyDelay);
        }
        else if (IsReadyToDelivery(other))
        {
            Debug.Log("Delivered package!!");
            hasPackage = false;
            Customer currentCustomer = other.GetComponent<Customer>();
            currentScore += currentCustomer.score;
            ScoreUI.UpdateText(currentScore);
            spriteRenderer.color = noPackageColor;
        }
    }

    private bool IsReadyToDelivery(Collider2D other)
    {
        return other.GetComponent<Customer>() && other.tag == currentPackageTag && hasPackage;
    }
}
