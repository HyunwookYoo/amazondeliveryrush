using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimeLimitManager : MonoBehaviour
{
    [SerializeField] TMP_Text timeLimitText;

    [SerializeField] float timeLimit = 60f;

    LevelLoader levelLoader;

    void Start()
    {
        timeLimitText.SetText("Time remain: " + timeLimit + " seconds");
        levelLoader = FindAnyObjectByType<LevelLoader>();
    }

    // Update is called once per frame
    void Update()
    {
        if (timeLimit > 0f)
        {
            timeLimit -= Time.deltaTime;
            string timeText = string.Format("{0:00}", timeLimit);
            timeLimitText.SetText("Time remain: " + timeText + " seconds");   
        }
        else
        {
            levelLoader.LoadNextLevel();
        }
        
    }
}
