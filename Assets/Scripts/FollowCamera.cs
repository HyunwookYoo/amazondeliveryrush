using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    [SerializeField] GameObject CarObject;

    // It should be same XY position to the car object
    void LateUpdate()
    {
        transform.position = CarObject.transform.position + new Vector3 (0f, 0f, -10f);
    }
}
