using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionToGame : MonoBehaviour
{
    [SerializeField] LevelLoader levelLoader;

    public void ButtonPressed()
    {
        Debug.Log("Button Pressed");
        levelLoader.LoadNextLevel();
    }

    public void TimeLimitLoadLevel()
    {
        levelLoader.LoadNextLevel();
    }
}
